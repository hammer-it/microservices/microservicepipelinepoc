const express = require('express')
const app = express()
const cors = require('cors')

app.use(express.json())
app.use(
    cors({ 
        origin: 'http://localhost:4200', 
        methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
        allowedHeaders: [
            'Content-Type', 
            'Authorization', 
            'Origin', 
            'x-access-token', 
            'XSRF-TOKEN'
        ], 
        preflightContinue: false 
    })
);

app.listen(8080, async () =>{
    try{
        console.log('Server at 8080');
    }catch(err){
        console.log(err);
        throw(err);
    }
})