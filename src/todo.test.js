const Todos = require('./todo');
const assert = require('assert').strict;
const fs = require('fs');

/*
Standard example of a unit mocha test
 - describe: This function is meant to group test so you have a better overview
 - it: This function contains our test code 

describe([String with Test Group Name], function() {
    it([String with Test Name], function() {
        [Test Code]
    });
});

*/

describe("integration test", function() {
    it("should be able to add TODOs and get my list (notStrictEqual test)", function() {
        let todos = new Todos();
        // notStrictEqual: This assert functions has a actual and expected value.
        // It throws a error if they are both the same
        assert.notStrictEqual(todos.list().length, 1);
    });

    it("should be able to add TODOs (strictEqual test)", function() {
        let todos = new Todos();
        todos.add("get up from bed");
        todos.add("make up bed");

        // strictEqual: This assert functions has a actual and expected value.
        // It throws a error if they are not the same
        assert.strictEqual(todos.list().length, 2);
    });

    it("should be able to add and complete TODOs (deepStrictEqual test)", function() {
        let todos = new Todos();
        assert.strictEqual(todos.list().length, 0);

        todos.add("run code");
        assert.strictEqual(todos.list().length, 1);

        // deepStrictEqual: recursively tests the expected and actual value, therefore it is used for lists and arrays
        assert.deepStrictEqual(todos.list(), [{title: "run code", completed: false}]);

        todos.add("test everything");
        assert.strictEqual(todos.list().length, 2);
        assert.deepStrictEqual(todos.list(),
            [
                { title: "run code", completed: false },
                { title: "test everything", completed: false }
            ]
        );

        todos.complete("run code");
        assert.deepStrictEqual(todos.list(),
            [
                { title: "run code", completed: true },
                { title: "test everything", completed: false }
            ]
        );
    });
});

describe("complete()", function() {
    it("should fail if there are no TODOs (assert.Throws test)", function() {
        let todos = new Todos();
        const expectedError = new Error("You have no TODOs stored. Why don't you add one first?");

        // This function was created so we can verify the errors that are thrown in our code. 
        // Its first argument is a function that contains the code that throws the error. 
        // The second argument is the error we are expecting to receive.
        assert.throws(() => {
            todos.complete("doesn't exist");
        }, expectedError);
    });
});

// How to test Callback functions and Promises
describe("saveToFile()", function() {
    // This Hook runs before each test is run
    beforeEach(function () {
        this.todos = new Todos();
        this.todos.add("save a CSV");
    });

    // This Hook runs when all tests have finished
    afterEach(function () {
        if (fs.existsSync("todos.csv")) {
            fs.unlinkSync("todos.csv");
        }
    });


    // Normaly you don't have a argument within the function of the it() function
    // This is only done when waiting on asyncronous function. Done is used to know when the asyncronous function is completed
    it("should save a single TODO with a callback function", function(done) {
        this.todos.saveToFile((err) => {
            // Check if file exists
            assert.strictEqual(fs.existsSync('todos.csv'), true);
            // Set expected value
            let expectedFileContents = "Title,Completed\nsave a CSV,false\n";
            // fs library has mostly asyncronous functions, but it does have a few syncronous counterparts 
            // readFileSync(): returns a buffer, which returns binary data
            let content = fs.readFileSync("todos.csv").toString(); 
            assert.strictEqual(content, expectedFileContents);
            // Done is called so that Mocha knows everything is done
            // By providing the done moethod with the err object it now has the possibility to throw an error if needed
            done(err);
        });
    });

    it("should save a single TODO with a Promise", function() {
        // No need for a try catch since Mocha knows when a Promise gets rejected
        return this.todos.saveToFileWithPromise().then(() => {
            assert.strictEqual(fs.existsSync('todos.csv'), true);
            let expectedFileContents = "Title,Completed\nsave a CSV,false\n";
            let content = fs.readFileSync("todos.csv").toString();
            assert.strictEqual(content, expectedFileContents);
        });
    });

    it("should save a single TODO that's completed", async function () {
        this.todos.complete("save a CSV");
        await this.todos.saveToFileWithPromise();

        assert.strictEqual(fs.existsSync('todos.csv'), true);
        let expectedFileContents = "Title,Completed\nsave a CSV,true\n";
        let content = fs.readFileSync("todos.csv").toString();
        assert.strictEqual(content, expectedFileContents);
    });
});